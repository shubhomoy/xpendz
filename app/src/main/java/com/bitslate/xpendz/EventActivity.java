package com.bitslate.xpendz;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.bitslate.xpendz.Objects.Event;
import com.bitslate.xpendz.Utils.Preference;
import com.bitslate.xpendz.Utils.VolleySingleton;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class EventActivity extends AppCompatActivity {

    private LinearLayout loadView;
    private ProgressBar progressbar;
    private LinearLayout eventTicket;
    private TextView eventTitle;
    private TextView eventDesc;
    private TextView eventPrice;
    private ImageView imagestrip1, imagestrip2;
    private Button register;

    private Event event;
    private String URL;
    private Preference prefs;
    private Context context;

    private void instantiate() {
        context = EventActivity.this;
        loadView = (LinearLayout) findViewById(R.id.load_view);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        progressbar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.MULTIPLY);
        eventTitle = (TextView) findViewById(R.id.event_title);
        eventDesc = (TextView) findViewById(R.id.event_desc);
        eventTicket = (LinearLayout) findViewById(R.id.event_ticket);
        eventPrice = (TextView) findViewById(R.id.event_price);
        register = (Button) findViewById(R.id.register);
        imagestrip1 = (ImageView) findViewById(R.id.image_strip1);
        imagestrip2 = (ImageView) findViewById(R.id.image_strip2);
        prefs = new Preference(this);
        URL = getIntent().getStringExtra("browser_url");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        instantiate();
        setEmptyView(0);
        showEvent(URL);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(EventActivity.this);
                builder.setTitle("Register");
                builder.setMessage("Registering will cost you Rs." + event.price + " Xpendz cash!");
                builder.setPositiveButton("Yes, please", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        registerForEvent(URL);
                        dialog.cancel();
                    }
                });

                builder.setNegativeButton("Nope", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.create().show();
            }
        });
    }

    private void registerForEvent(String url) {
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                View view = LayoutInflater.from(context).inflate(R.layout.custom_trans_success_layout, null);
                builder.setView(view);
                final TextView title = (TextView) view.findViewById(R.id.textTitle);
                final Button done = (Button) view.findViewById(R.id.done);
                title.setText("Contribution Successful");
                done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MainActivity.fetchUser();
                        finish();
                    }
                });
                builder.create().show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(EventActivity.this, "Connection Timeout", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", String.valueOf(prefs.getUser().id));
                return params;
            }
        };
        VolleySingleton.getInstance().getRequestQueue().add(request);
    }

    private void showEvent(String url) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = new Gson();
                try {
                    setEmptyView(1);
                    event = gson.fromJson(response.getString("event"), Event.class);
                    eventTitle.setText(event.event_name);
                    eventDesc.setText(event.event_description);
                    eventPrice.setText("Price: Rs." + event.price);
                } catch (JSONException e) {
                    Toast.makeText(EventActivity.this, "Connection Timeout", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("options", error.toString());
                Toast.makeText(EventActivity.this, "Connection Timeout", Toast.LENGTH_SHORT).show();
            }
        });
        VolleySingleton.getInstance().getRequestQueue().add(request);
    }


    void setEmptyView(int size) {
        if (size == 0) {
            loadView.setVisibility(View.VISIBLE);
            eventTicket.setVisibility(View.GONE);
            register.setVisibility(View.GONE);
            imagestrip1.setVisibility(View.GONE);
            imagestrip2.setVisibility(View.GONE);
        } else {
            loadView.setVisibility(View.GONE);
            eventTicket.setVisibility(View.VISIBLE);
            register.setVisibility(View.VISIBLE);
            imagestrip1.setVisibility(View.VISIBLE);
            imagestrip2.setVisibility(View.VISIBLE);
        }
    }
}
