package com.bitslate.xpendz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.bitslate.xpendz.Adapters.IntroPageAdapter;
import com.bitslate.xpendz.Fragments.IntroFive;
import com.bitslate.xpendz.Fragments.IntroFour;
import com.bitslate.xpendz.Fragments.IntroOne;
import com.bitslate.xpendz.Fragments.IntroThree;
import com.bitslate.xpendz.Fragments.IntroTwo;
import com.bitslate.xpendz.Utils.Preference;
import com.bitslate.xpendz.Utils.ProgressPageIndicator;

import java.util.ArrayList;

public class IntroActivity extends AppCompatActivity {

    private ViewPager pager;
    private ArrayList<Fragment> fragments;
    private Preference prefs;
    private ProgressPageIndicator indicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        instantiate();

        indicator = (ProgressPageIndicator) findViewById(R.id.pageIndicator);

        pager.setAdapter(new IntroPageAdapter(getSupportFragmentManager(), fragments));
        indicator.setViewPager(pager);

        CustomOnPageChangeListener adapter = new CustomOnPageChangeListener();
        pager.addOnPageChangeListener(adapter);

    }

    void instantiate() {
        pager = (ViewPager) findViewById(R.id.intro_pager);
        prefs = new Preference(this);
        fragments = new ArrayList<>();
        fragments.add(new IntroOne());
        fragments.add(new IntroTwo());
        fragments.add(new IntroThree());
        fragments.add(new IntroFour());
        fragments.add(new IntroFive());

        if (prefs.getUser().id != 0) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }


    private class CustomOnPageChangeListener extends ViewPager.SimpleOnPageChangeListener {

        @Override
        public void onPageSelected(int position) {

            switch (position) {
                case 0:
                    indicator.setViewPager(pager, 0);
                    indicator.setFillColor(getResources().getColor(R.color.colorPrimaryDark));
                    break;
                case 1:
                    indicator.setViewPager(pager, 1);
                    indicator.setFillColor(getResources().getColor(R.color.colorPrimaryDark));
                    break;
                case 2:
                    indicator.setViewPager(pager, 2);
                    indicator.setFillColor(getResources().getColor(R.color.colorPrimaryDark));
                    break;
                case 3:
                    indicator.setViewPager(pager, 3);
                    indicator.setFillColor(getResources().getColor(R.color.colorPrimaryDark));
                    break;
                case 4:
                    indicator.setViewPager(pager, 4);
                    indicator.setFillColor(getResources().getColor(R.color.colorPrimaryDark));
                default:
                    break;
            }
            super.onPageSelected(position);
        }
    }
}