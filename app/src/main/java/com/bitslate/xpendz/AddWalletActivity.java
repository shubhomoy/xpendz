package com.bitslate.xpendz;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bitslate.xpendz.Utils.Config;
import com.bitslate.xpendz.Utils.Preference;
import com.bitslate.xpendz.Utils.VolleySingleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AddWalletActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private Spinner cardType;
    private EditText cardNumber1;
    private EditText cardNumber2;
    private EditText cardNumber3;
    private EditText cardNumber4;
    private EditText cardMonth;
    private EditText cardYearEt;
    private EditText cardHolderNameEt;
    private EditText cvvEt;
    private EditText amountEt;
    private Button depositBtn;

    Preference prefs;

    private void instantiate() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        cardType = (Spinner) findViewById(R.id.card_type);
        cardNumber1 = (EditText) findViewById(R.id.card_number_1);
        cardNumber2 = (EditText) findViewById(R.id.card_number_2);
        cardNumber3 = (EditText) findViewById(R.id.card_number_3);
        cardNumber4 = (EditText) findViewById(R.id.card_number_4);
        cardMonth = (EditText) findViewById(R.id.card_month);
        cardYearEt = (EditText) findViewById(R.id.card_year_et);
        cardHolderNameEt = (EditText) findViewById(R.id.card_holder_name_et);
        cvvEt = (EditText) findViewById(R.id.cvv_et);
        amountEt = (EditText) findViewById(R.id.amount_et);
        depositBtn = (Button) findViewById(R.id.deposit_btn);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Add Xpendz Cash");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        prefs = new Preference(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_wallet);
        instantiate();

        ArrayList<String> cardTypes = new ArrayList<>();
        cardTypes.add("Visa");
        cardTypes.add("Master Card");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, cardTypes);
        cardType.setAdapter(adapter);

        depositBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (amountEt.getText().toString().trim().length() > 0) {
                    final ProgressDialog progressDialog = new ProgressDialog(AddWalletActivity.this);
                    progressDialog.setMessage("Please wait");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    String url = Config.URL + "/add/wallet";
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            int totalAmount = prefs.getUser().wallet + Integer.parseInt(amountEt.getText().toString().trim());
                            MainActivity.wallet.setText("Rs." + totalAmount);
                            progressDialog.dismiss();
                            AlertDialog.Builder builder = new AlertDialog.Builder(AddWalletActivity.this);
                            View view = LayoutInflater.from(AddWalletActivity.this).inflate(R.layout.custom_trans_success_layout, null);
                            builder.setView(view);
                            final Button button = (Button) view.findViewById(R.id.done);
                            button.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    finish();
                                }
                            });
                            builder.create().show();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("option", error.toString());
                            progressDialog.dismiss();
                            Toast.makeText(AddWalletActivity.this, "Connection Timeout", Toast.LENGTH_LONG).show();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            HashMap<String, String> params = new HashMap<String, String>();
                            params.put("user_id", String.valueOf(prefs.getUser().id));
                            params.put("amount", amountEt.getText().toString());
                            return params;
                        }
                    };
                    VolleySingleton.getInstance().getRequestQueue().add(stringRequest);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
