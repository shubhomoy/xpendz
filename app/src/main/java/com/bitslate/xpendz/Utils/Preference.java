package com.bitslate.xpendz.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.bitslate.xpendz.Objects.User;

/**
 * Created by shubhomoy on 9/1/16.
 */
public class Preference {
    Context context;
    SharedPreferences prefs;

    public Preference(Context context) {
        this.context = context;
        prefs = context.getSharedPreferences("xpendz", Context.MODE_PRIVATE);
    }

    public void setUser(User user) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("user_id", user.id);
        editor.putString("name", user.name);
        editor.putString("phone", user.phone);
        editor.putInt("wallet", user.wallet);
        editor.commit();
    }

    public User getUser() {
        User user = new User();
        user.id = prefs.getInt("user_id", 0);
        user.name = prefs.getString("name", null);
        user.phone = prefs.getString("phone", null);
        user.wallet = prefs.getInt("wallet", 0);
        return user;
    }

    public void clear() {
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();
    }

    public String getGcmId() {
        return prefs.getString(XpendzGCM.PROPERTY_REG_ID,"");
    }

    public void getPaid(int id, int amount) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("already_"+String.valueOf(id), amount);
        editor.commit();
    }

    public void payTo(int id, int amount) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(String.valueOf(id), amount);
        editor.commit();
    }

    public int alreadyPaidTo(int id) {
        return prefs.getInt(String.valueOf(id), 0);
    }

    public int alreadyPaidBy(int id) {
        return prefs.getInt("already_"+String.valueOf(id), 0);
    }
}
