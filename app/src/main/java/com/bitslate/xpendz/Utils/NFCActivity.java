package com.bitslate.xpendz.Utils;

import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bitslate.xpendz.GroupCreatedActivity;
import com.bitslate.xpendz.Objects.GroupMember;
import com.bitslate.xpendz.QuickPayActivity;
import com.bitslate.xpendz.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Created by shubhomoy on 13/3/16.
 */
public class NFCActivity extends AppCompatActivity {

    Preference prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blank);

        prefs = new Preference(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Intent intent = getIntent();
        String action = intent.getAction();

        if (action.equals(NfcAdapter.ACTION_NDEF_DISCOVERED)) {

            Parcelable[] parcelables = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            if (parcelables != null) {
                NdefMessage inNdefMessage = (NdefMessage) parcelables[0];
                NdefRecord[] records = inNdefMessage.getRecords();
                NdefRecord record = records[0];

                String message = new String(record.getPayload());
                StringTokenizer stringTokenizer = new StringTokenizer(message, " ");
                if(stringTokenizer.nextToken().equals("grouppay")) {
                    int from_id = Integer.parseInt(stringTokenizer.nextToken());
                    String name = stringTokenizer.nextToken();
                    int share = Integer.parseInt(stringTokenizer.nextToken());

                    GroupCreatedActivity.list.add(new GroupMember(from_id, name, share));
                    GroupCreatedActivity.adapter.notifyDataSetChanged();
                    GroupCreatedActivity.amount += share;


//                    if (GroupCreatedActivity.amount == GroupCreatedActivity.totAmt)
//                        GroupCreatedActivity.payBtn.setEnabled(true);
//                    else
//                        GroupCreatedActivity.payBtn.setEnabled(false);
                    finish();
                }else{
                    startActivity(new Intent(this, QuickPayActivity.class));
                    int from_id = Integer.parseInt(stringTokenizer.nextToken());
                    int amount = Integer.parseInt(stringTokenizer.nextToken());
                    getCode(from_id, amount);
                }
            }
        }
    }

    public void getCode(final int from_id, final int amount) {
        String url = Config.URL + "/pay/getcode";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    QuickPayActivity.lendAmount.setText("" + amount);
                    QuickPayActivity.lendCode.setText(jsonObject.getString("code"));
                    finish();
                } catch (JSONException e) {
                    Toast.makeText(NFCActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("error", error.toString());
                Toast.makeText(NFCActivity.this, "Connection Timeout", Toast.LENGTH_LONG).show();
                finish();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("from_id", String.valueOf(from_id));
                params.put("amount", String.valueOf(amount));
                params.put("to_id", String.valueOf(prefs.getUser().id));
                return params;
            }
        };
        VolleySingleton.getInstance().getRequestQueue().add(stringRequest);
    }
}
