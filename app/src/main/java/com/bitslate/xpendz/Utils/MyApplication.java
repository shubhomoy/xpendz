package com.bitslate.xpendz.Utils;

import android.app.Application;
import android.content.Context;

/**
 * Created by shubhomoy on 9/1/16.
 */
public class MyApplication extends Application {
    private static MyApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static MyApplication getInstance() {
        return instance;
    }

    public static Context getAppContext() {
        return instance.getApplicationContext();
    }
}
