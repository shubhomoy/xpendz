package com.bitslate.xpendz.Utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by shubhomoy on 8/1/16.
 */
public class XpendzDbAdapter {

    private final static String DATABASE_NAME = "xpendz_db";
    private final static String DATABASE_TABLE_ITEM = "items";
    private final static int DATABASE_VERSION = 1;

    private SQLiteDatabase db;

   // private final Context context;

    public static final String KEY_ID = "_id";
    public static final String KEY_NOTES = "note_text";
    public static final String KEY_DATETIME = "note_datetime";
    public static final String KEY_TYPE = "note_type";
    public static final String KEY_IS_PASSWORD = "password_enabled";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_PASSWORD_TITLE = "password_title";
    public static final String KEY_COLOR = "color";

}
