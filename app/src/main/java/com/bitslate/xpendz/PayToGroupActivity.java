package com.bitslate.xpendz;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import com.bitslate.xpendz.Utils.Preference;

public class PayToGroupActivity extends AppCompatActivity implements NfcAdapter.CreateNdefMessageCallback, NfcAdapter.OnNdefPushCompleteCallback {

    private EditText share;
    private Context context;
    private Preference prefs;

    private NfcAdapter nfcAdapter;
    private PackageManager packageManager;

    void instatntiate() {
        context = PayToGroupActivity.this;
        share = (EditText) findViewById(R.id.share);
        prefs = new Preference(context);
        nfcAdapter = NfcAdapter.getDefaultAdapter(context);
        packageManager = this.getPackageManager();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_to_group);

        instatntiate();

        if (!packageManager.hasSystemFeature(PackageManager.FEATURE_NFC))
            Toast.makeText(context, "NO NFC FOUND!", Toast.LENGTH_SHORT).show();
        else {
            if (!nfcAdapter.isEnabled())
                Toast.makeText(context, "Enable NFC", Toast.LENGTH_SHORT).show();
            else {
                nfcAdapter.setNdefPushMessageCallback(this, this);
                nfcAdapter.setOnNdefPushCompleteCallback(this, this);
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Intent groupIntent = new Intent(context, GroupCreatedActivity.class);
        setIntent(groupIntent);
    }

    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {
        String text = "grouppay " + prefs.getUser().id + " " + prefs.getUser().name + " " + share.getText().toString();
        byte[] bytes = text.getBytes();

        NdefRecord record = new NdefRecord(
                NdefRecord.TNF_MIME_MEDIA,
                "text/plain".getBytes(),
                new byte[]{},
                bytes);
        return new NdefMessage(record);
    }

    @Override
    public void onNdefPushComplete(NfcEvent event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                View view = LayoutInflater.from(context).inflate(R.layout.custom_trans_success_layout, null);
//                builder.setView(view);
//                final TextView title = (TextView) view.findViewById(R.id.textTitle);
//                final Button done = (Button) view.findViewById(R.id.done);
//                title.setText("Contribution Successful");
//                done.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        finish();
//                    }
//                });
//                builder.create().show();
            }
        });

    }
}
