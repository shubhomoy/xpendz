package com.bitslate.xpendz;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bitslate.xpendz.Adapters.ReminderAdapter;
import com.bitslate.xpendz.Objects.Reminder;
import com.bitslate.xpendz.Objects.User;
import com.bitslate.xpendz.Utils.Config;
import com.bitslate.xpendz.Utils.Preference;
import com.bitslate.xpendz.Utils.VolleySingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by shubhomoy on 11/1/16.
 */
public class ReminderActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    Toolbar toolbar;
    Context context;
    LinearLayout emptyView;
    ReminderAdapter adapter;
    ArrayList<Reminder> list;
    Preference prefs;

    void instantiate() {
        context = this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Reminders");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        emptyView = (LinearLayout) findViewById(R.id.empty_view);

        list = new ArrayList<>();
        adapter = new ReminderAdapter(context, list);
        prefs = new Preference(context);
        recyclerView.setAdapter(adapter);
        showReminders();
    }

    void showReminders() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching reminders");
        progressDialog.setCancelable(false);
        progressDialog.show();
        String url = Config.URL + "/show/reminders?user_id=" + prefs.getUser().id;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response.getString("data"));
                    JSONArray jsonArray = new JSONArray(jsonObject.getString("reminders"));
                    Gson gson = new Gson();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Reminder reminder = gson.fromJson(jsonArray.getJSONObject(i).toString(), Reminder.class);
                        list.add(reminder);
                    }
                    setEmptyView(jsonArray.length());
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.d("option", error.toString());
                Toast.makeText(context, "Connection Timeout", Toast.LENGTH_LONG).show();
            }
        });
        VolleySingleton.getInstance().getRequestQueue().add(jsonObjectRequest);
    }

    void setEmptyView(int length) {
        if (length == 0) {
            emptyView.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder);
        instantiate();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.help_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
}
