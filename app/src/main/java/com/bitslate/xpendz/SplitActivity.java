package com.bitslate.xpendz;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.bitslate.xpendz.Adapters.FriendListAdapter;
import com.bitslate.xpendz.Objects.Item;
import com.bitslate.xpendz.Objects.User;
import com.bitslate.xpendz.Utils.Config;
import com.bitslate.xpendz.Utils.VolleySingleton;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SplitActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView totExp, itemName;
    FloatingActionButton btnAddFriend;
    CoordinatorLayout root;
    RecyclerView friendList;
    Intent intent;
    Item item;
    ArrayList<User> list;
    Context context;
    FriendListAdapter adapter;
    SwipeRefreshLayout swipeRefreshLayout;
    ProgressDialog progressDialog;

    int paid_left = 0, share_left = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_split);
        instantiate();
        totExp.setText(String.valueOf(item.cost));
        btnAddFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, 1);
            }
        });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                swipeRefreshLayout.setEnabled(false);
                showFriends();
            }
        });
    }

    void addFriend(final String phone) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Adding friend");
        progressDialog.show();
        String url = Config.URL + "/add/friend";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(SplitActivity.this);
                    if (jsonObject.getString("msg").equals("NA")) {
                        builder.setTitle("Not found");
                        builder.setMessage("This user is not registered with Xpendz. Invite him/her to install and sign up in Xpendz.");
                        builder.setPositiveButton("Ok", null);
                        builder.setNeutralButton("Help", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                showInviteHelp();
                            }
                        });
                        builder.create().show();
                    } else if (jsonObject.getString("msg").equals("invalid")) {
                        builder.setTitle("Invalid");
                        builder.setMessage("You cannot add your own number");
                        builder.setPositiveButton("Ok", null);
                        builder.create().show();
                    } else if (jsonObject.getString("msg").equals("valid")) {
                        Gson gson = new Gson();
                        User user = gson.fromJson(jsonObject.getString("user").toString(), User.class);
                        user.pivot = user.new Pivot();
                        user.pivot.share = 0;
                        user.pivot.paid = 0;
                        list.add(user);
                        adapter.notifyDataSetChanged();
                        Snackbar.make(root, "Friend added", Snackbar.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    Snackbar.make(root, "Something went wrong", Snackbar.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.d("option", error.toString());
                Snackbar.make(root, "Something went wrong", Snackbar.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("phone", phone);
                params.put("item_id", String.valueOf(item.id));
                return params;
            }
        };
        VolleySingleton.getInstance().getRequestQueue().add(stringRequest);
    }

    void instantiate() {
        context = this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        root = (CoordinatorLayout) findViewById(R.id.root);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        itemName = (TextView) toolbar.findViewById(R.id.item_name);
        totExp = (TextView) findViewById(R.id.tot_exp);
        btnAddFriend = (FloatingActionButton) findViewById(R.id.add_friend);
        friendList = (RecyclerView) findViewById(R.id.friend_list);
        friendList.setLayoutManager(new LinearLayoutManager(this));
        friendList.setHasFixedSize(true);
        intent = getIntent();
        item = new Item();
        item.id = intent.getIntExtra("item_id", 0);
        item.cost = intent.getIntExtra("cost", 0);
        item.name = intent.getStringExtra("item_name");
        list = new ArrayList<User>();
        adapter = new FriendListAdapter(context, list, item);
        friendList.setAdapter(adapter);
        itemName.setText(item.name);

        Typeface regularTypeface = Typeface.createFromAsset(getAssets(), "fonts/regular.ttf");
        itemName.setTypeface(regularTypeface);
        totExp.setTypeface(regularTypeface);

        swipeRefreshLayout.setEnabled(false);
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });
        showFriends();

    }

    void showInviteHelp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Invite Friends");
        builder.setMessage("Your friends need to register with Xpendz in order to add them. Invite your friends to install Xpendz");
        builder.setPositiveButton("Ok", null);
        builder.create().show();
    }

    void showFriends() {
        list.removeAll(list);
        list.clear();
        String url = Config.URL + "/show/item/" + item.id;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                swipeRefreshLayout.setRefreshing(false);
                swipeRefreshLayout.setEnabled(true);
                Gson gson = new Gson();
                try {
                    Item item = gson.fromJson(response.getString("item"), Item.class);
                    if(item!=null) {
                        for (int i = 0; i < item.users.size(); i++) {
                            paid_left += item.users.get(i).pivot.paid;
                            share_left += item.users.get(i).pivot.share;
                            list.add(item.users.get(i));
                        }
                        adapter.paid_left = item.cost - paid_left;
                        adapter.share_left = item.cost - share_left;
                        adapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    Snackbar.make(root, "Something went wrong", Snackbar.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);
                swipeRefreshLayout.setEnabled(true);
                Log.d("option", error.toString());
                Snackbar.make(root, "Connection timeout", Snackbar.LENGTH_LONG).show();
            }
        });
        VolleySingleton.getInstance().getRequestQueue().add(jsonObjectRequest);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.split_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 1) {

            Uri contactData = data.getData();
            Cursor c = managedQuery(contactData, null, null, null, null);
            if (c.moveToFirst()) {
                String id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                if (hasPhone.equalsIgnoreCase("1")) {
                    Cursor phones = getContentResolver().query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                            null, null);
                    phones.moveToFirst();
                    String cNumber = phones.getString(phones.getColumnIndex("data1"));
                    cNumber = cNumber.trim();
                    if (cNumber.charAt(0) == '+') {
                        cNumber = cNumber.substring(3);
                    } else if (cNumber.charAt(0) == '0') {
                        cNumber = cNumber.substring(1);
                    }
                    cNumber = cNumber.replaceAll("\\s", "");
                    cNumber = cNumber.replaceAll("-", "");
                    addFriend(cNumber);
                }
            }
        }
    }


}
