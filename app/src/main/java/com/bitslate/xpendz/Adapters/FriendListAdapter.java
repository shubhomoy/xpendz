package com.bitslate.xpendz.Adapters;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bitslate.xpendz.Objects.Item;
import com.bitslate.xpendz.Objects.User;
import com.bitslate.xpendz.R;
import com.bitslate.xpendz.Utils.Config;
import com.bitslate.xpendz.Utils.VolleySingleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ddvlslyr on 9/1/16.
 */
public class FriendListAdapter extends RecyclerView.Adapter<FriendListAdapter.FriendViewHolder> {

    Context context;
    ArrayList<User> list;
    Item item;
    public int paid_left, share_left, paid_temp, share_temp;

    public FriendListAdapter(Context context, ArrayList<User> list, Item item) {
        this.context = context;
        this.list = list;
        this.item = item;
    }

    @Override
    public FriendViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.custom_friend_item_row, parent, false);
        return new FriendViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FriendViewHolder holder, int position) {
        final User user = list.get(position);
        holder.name.setText(user.name);
        if (user.pivot != null) {
            holder.amt.setText("Paid " + String.valueOf(user.pivot.paid));
            holder.state.setText("Share " + String.valueOf(user.pivot.share));
        } else {
            holder.amt.setText("Paid 0");
            holder.state.setText("Share 0");
        }
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View v = LayoutInflater.from(context).inflate(R.layout.custom_split_dialog, null);
                final EditText paid = (EditText) v.findViewById(R.id.paid);
                final EditText share = (EditText) v.findViewById(R.id.share);
                final TextView shareDynamic = (TextView)v.findViewById(R.id.share_dynamic);
                final TextView paidDynamic = (TextView)v.findViewById(R.id.paid_dynamic);
                paid.setText(String.valueOf(user.pivot.paid));
                share.setText(String.valueOf(user.pivot.share));
                paid_temp = user.pivot.paid;
                share_temp = user.pivot.share;
                shareDynamic.setText(String.valueOf(share_left) + " left");
                paidDynamic.setText(String.valueOf(paid_left) + " left");

                paid.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if (charSequence.toString().isEmpty()) {
                            paid_temp = 0;
                            paidDynamic.setText(String.valueOf(paid_left + user.pivot.paid) + " left");
                        }else {
                            paid_temp = Integer.parseInt(charSequence.toString());
                            paidDynamic.setText(String.valueOf(paid_left + user.pivot.paid - paid_temp) + " left");
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });

                share.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if (charSequence.toString().isEmpty()) {
                            share_temp = 0;
                            shareDynamic.setText(String.valueOf(share_left + user.pivot.share) + " left");
                        }else {
                            share_temp = Integer.parseInt(charSequence.toString());
                            shareDynamic.setText(String.valueOf(share_left + user.pivot.share - share_temp) + " left");
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(user.name);
                builder.setView(v);
                builder.setPositiveButton("Done", null);
                builder.setNegativeButton("Cancel", null);
                builder.setNeutralButton("Help", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        showHelp();
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new DoneListener(alertDialog, user));
            }
        });
    }

    class DoneListener implements View.OnClickListener {
        private final Dialog dialog;
        User user;
        public DoneListener(Dialog dialog, User user) {
            this.dialog = dialog;
            this.user = user;
        }
        @Override
        public void onClick(View view) {
            if((item.cost - share_temp) < 0 || (item.cost - paid_temp) < 0) {
                Toast.makeText(context, "Invalid input", Toast.LENGTH_LONG).show();
            }else {
                editFriend(user);
                dialog.dismiss();
            }
        }
    }

    void showHelp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Help");
        View v = LayoutInflater.from(context).inflate(R.layout.split_dialog_help, null);
        builder.setView(v);
        builder.setPositiveButton("Ok", null);
        builder.create().show();
    }

    void editFriend(final User user) {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Updating "+user.name);
        progressDialog.setCancelable(false);
        progressDialog.show();
        String url = Config.URL + "/edit/friend/"+user.id+"/item/"+item.id;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                user.pivot = user.new Pivot();
                user.pivot.paid = paid_temp;
                user.pivot.share = share_temp;

                share_left = 0;
                paid_left = 0;
                for(int i=0; i<list.size(); i++) {
                    paid_left += list.get(i).pivot.paid;
                    share_left += list.get(i).pivot.share;
                }
                paid_left = item.cost - paid_left;
                share_left = item.cost - share_left;
                FriendListAdapter.this.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.d("option", error.toString());
                Toast.makeText(context, "Connection Timeout", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("paid", String.valueOf(paid_temp));
                params.put("share", String.valueOf(share_temp));
                return params;
            }
        };
        VolleySingleton.getInstance().getRequestQueue().add(stringRequest);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class FriendViewHolder extends RecyclerView.ViewHolder {
        private TextView name, amt, state;
        RelativeLayout item;

        public FriendViewHolder(View itemView) {
            super(itemView);
            item = (RelativeLayout) itemView.findViewById(R.id.root);
            name = (TextView) itemView.findViewById(R.id.name);
            amt = (TextView) itemView.findViewById(R.id.cost);
            state = (TextView) itemView.findViewById(R.id.state);

            Typeface regularTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/regular.ttf");
            name.setTypeface(regularTypeface);
            amt.setTypeface(regularTypeface);
            state.setTypeface(regularTypeface);
        }
    }
}
