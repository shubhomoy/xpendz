package com.bitslate.xpendz.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bitslate.xpendz.Fragments.RecordsFragment;
import com.bitslate.xpendz.MainActivity;
import com.bitslate.xpendz.Objects.Item;
import com.bitslate.xpendz.R;
import com.bitslate.xpendz.SplitActivity;
import com.bitslate.xpendz.Utils.Config;
import com.bitslate.xpendz.Utils.VolleySingleton;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ddvlslyr on 8/1/16.
 */
public class ItemsActivityAdapter extends RecyclerView.Adapter<ItemsActivityAdapter.ItemViewHolder> {


    private Context context;
    ArrayList<Item> list;
    ProgressDialog progressDialog;

    public ItemsActivityAdapter(Context context, ArrayList<Item> list) {
        this.context = context;
        this.list = list;
        progressDialog = new ProgressDialog(context);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.custom_item_row, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        final Item item = list.get(position);
        holder.name.setText(item.name);
        holder.cost.setText(String.valueOf(item.cost));
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                View v = LayoutInflater.from(context).inflate(R.layout.custom_dialog_list, null);
                ListView listView = (ListView) v.findViewById(R.id.listview);
                String[] list_items = {"Delete Item"};
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_expandable_list_item_1, list_items);
                listView.setAdapter(arrayAdapter);
                builder.setView(v);
                final AlertDialog dialog = builder.create();
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        switch (i) {
                            case 0:
                                confirmDelete(item);
                                dialog.dismiss();
                                break;
                        }
                    }
                });
                dialog.show();
            }
        });

        holder.item.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                View v = LayoutInflater.from(context).inflate(R.layout.custom_dialog_list, null);
                ListView listView = (ListView) v.findViewById(R.id.listview);
                String[] list_items = {"Delete Item"};
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_expandable_list_item_1, list_items);
                listView.setAdapter(arrayAdapter);
                builder.setView(v);
                final AlertDialog dialog = builder.create();
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        switch (i) {
                            case 0:
                                confirmDelete(item);
                                dialog.dismiss();
                                break;
                        }
                    }
                });
                dialog.show();
                return false;
            }
        });

        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SplitActivity.class);

                intent.putExtra("item_id", item.id);
                intent.putExtra("cost", item.cost);
                intent.putExtra("item_name", item.name);
                ((MainActivity) context).startActivityForResult(intent, 1);
            }
        });

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SplitActivity.class);

                intent.putExtra("item_id", item.id);
                intent.putExtra("cost", item.cost);
                intent.putExtra("item_name", item.name);
                ((MainActivity) context).startActivityForResult(intent, 1);
            }
        });
    }

    void confirmDelete(final Item item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Confirm");
        builder.setMessage("Do you want to delete this item?");
        builder.setNegativeButton("No", null);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                deleteItem(item);
            }
        });
        builder.create().show();
    }

    void deleteItem(final Item item) {
        progressDialog.setMessage("Removing item");
        progressDialog.setCancelable(false);
        progressDialog.show();
        String url = Config.URL + "/remove/item/" + item.id;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                list.remove(item);
                ItemsActivityAdapter.this.notifyDataSetChanged();
                progressDialog.dismiss();
                MainActivity.pager.setCurrentItem(0, true);
                MainActivity.refreshRecords();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("option", error.toString());
                progressDialog.dismiss();
                Toast.makeText(context, "Connection Timeout", Toast.LENGTH_LONG).show();
            }
        });
        VolleySingleton.getInstance().getRequestQueue().add(jsonObjectRequest);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView name, cost;
        RelativeLayout item;
        ImageView view;
        ImageView delete;

        public ItemViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            cost = (TextView) itemView.findViewById(R.id.cost);
            item = (RelativeLayout) itemView.findViewById(R.id.root);
            view = (ImageView)itemView.findViewById(R.id.view);
            delete = (ImageView)itemView.findViewById(R.id.delete);

            Typeface regularTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/regular.ttf");
            name.setTypeface(regularTypeface);
            cost.setTypeface(regularTypeface);
        }
    }
}
