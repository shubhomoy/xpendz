package com.bitslate.xpendz.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bitslate.xpendz.GroupCreatedActivity;
import com.bitslate.xpendz.Objects.GroupMember;
import com.bitslate.xpendz.R;

import java.util.ArrayList;

/**
 * Created by shubhomoy on 13/3/16.
 */
public class GroupListAdapter extends RecyclerView.Adapter<GroupListAdapter.GroupListViewHolder> {

    Context context;
    ArrayList<GroupMember> list;

    public GroupListAdapter(Context context, ArrayList<GroupMember> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public GroupListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GroupListViewHolder(LayoutInflater.from(context).inflate(R.layout.custom_group_member, parent, false));
    }

    @Override
    public void onBindViewHolder(GroupListViewHolder holder, final int position) {
        final GroupMember groupMember = list.get(position);
        holder.name.setText(groupMember.name);
        holder.amount.setText(String.valueOf(groupMember.amount));

        if (position == 0)
            holder.delete.setVisibility(View.GONE);
        else
            holder.delete.setVisibility(View.VISIBLE);

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((GroupCreatedActivity) context).removeItems(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class GroupListViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView amount;
        ImageView delete;

        public GroupListViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            amount = (TextView) itemView.findViewById(R.id.amount);
            delete = (ImageView) itemView.findViewById(R.id.delete);
        }
    }
}
