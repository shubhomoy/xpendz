package com.bitslate.xpendz.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bitslate.xpendz.Objects.Reminder;
import com.bitslate.xpendz.Objects.User;
import com.bitslate.xpendz.R;
import com.bitslate.xpendz.Utils.Config;
import com.bitslate.xpendz.Utils.VolleySingleton;

import java.util.ArrayList;

/**
 * Created by shubhomoy on 11/1/16.
 */
public class ReminderAdapter extends RecyclerView.Adapter<ReminderAdapter.ReminderViewHolder> {

    ArrayList<Reminder> list;
    Context context;

    public ReminderAdapter(Context context, ArrayList<Reminder> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ReminderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.custom_reminder_row, parent, false);
        ReminderViewHolder holder = new ReminderViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ReminderViewHolder holder, int position) {
        final Reminder reminder = list.get(position);
        holder.textView.setText("You have a due to pay "+list.get(position).from.name);
        holder.dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissReminder(reminder);
            }
        });
    }

    void dismissReminder(Reminder reminder) {
        list.remove(reminder);
        ReminderAdapter.this.notifyDataSetChanged();
        String url = Config.URL + "/remove/reminder/"+reminder.id;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("option", error.toString());
                Toast.makeText(context, "Connection timeout", Toast.LENGTH_LONG).show();
            }
        });
        VolleySingleton.getInstance().getRequestQueue().add(stringRequest);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ReminderViewHolder extends RecyclerView.ViewHolder {

        TextView textView;
        Button dismiss;

        public ReminderViewHolder(View itemView) {
            super(itemView);
            dismiss = (Button)itemView.findViewById(R.id.dismiss);
            textView = (TextView)itemView.findViewById(R.id.text);
        }
    }
}
