package com.bitslate.xpendz.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bitslate.xpendz.MainActivity;
import com.bitslate.xpendz.Objects.User;
import com.bitslate.xpendz.R;
import com.bitslate.xpendz.Utils.Config;
import com.bitslate.xpendz.Utils.Preference;
import com.bitslate.xpendz.Utils.VolleySingleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ddvlslyr on 8/1/16.
 */
public class MainPageAdapter extends RecyclerView.Adapter<MainPageAdapter.MainViewHolder> {

    private Context context;
    ArrayList<User> list;
    Preference prefs;

    public MainPageAdapter(Context context, ArrayList<User> list) {
        this.context = context;
        this.list = list;
        prefs = new Preference(context);
    }


    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(context).inflate(R.layout.custom_main_item_row, parent, false);
        return new MainViewHolder(row);
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        final User user = list.get(position);
        holder.name.setText(user.name);
        if (user.my_stat < 0) {
            holder.state.setText("You Borrowed");
            holder.amt.setText(String.valueOf(-user.my_stat));
            holder.amt.setTextColor(Color.parseColor("#DD2C00"));
            holder.state.setTextColor(Color.parseColor("#DD2C00"));
            holder.item.setOnClickListener(null);
            holder.remindBtn.setVisibility(View.GONE);
            holder.payNowBtn.setVisibility(View.VISIBLE);
            holder.payNowBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showPayNowOptions(user, -user.my_stat);
                }
            });
        } else {
            holder.state.setText("You Lent");
            holder.amt.setText(String.valueOf(user.my_stat));
            holder.amt.setTextColor(Color.parseColor("#1B5E20"));
            holder.state.setTextColor(Color.parseColor("#1B5E20"));
            holder.remindBtn.setVisibility(View.VISIBLE);
            holder.payNowBtn.setVisibility(View.GONE);
            holder.remindBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendReminder(user.id);
                }
            });
        }
    }

    void sendReminder(final int to_id) {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Sending Reminder");
        progressDialog.setCancelable(false);
        progressDialog.show();
        String url = Config.URL + "/add/reminder";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Toast.makeText(context, "Reminder sent", Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("option", error.toString());
                progressDialog.dismiss();
                Toast.makeText(context, "Connection Timeout", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("from_id", String.valueOf(prefs.getUser().id));
                params.put("to_id", String.valueOf(to_id));
                return params;
            }
        };
        VolleySingleton.getInstance().getRequestQueue().add(stringRequest);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MainViewHolder extends RecyclerView.ViewHolder {
        private TextView name, amt, state;
        RelativeLayout item, remindBtn, payNowBtn;

        public MainViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            amt = (TextView) itemView.findViewById(R.id.cost);
            state = (TextView) itemView.findViewById(R.id.state);
            item = (RelativeLayout) itemView.findViewById(R.id.root);
            remindBtn = (RelativeLayout) itemView.findViewById(R.id.reminder_btn);
            payNowBtn = (RelativeLayout) itemView.findViewById(R.id.pay_now_btn);

            Typeface regularTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/regular.ttf");
            name.setTypeface(regularTypeface);
            amt.setTypeface(regularTypeface);
            state.setTypeface(regularTypeface);
        }
    }

    void showPayNowOptions(final User user, final int amount) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Pay Via");
        View v = LayoutInflater.from(context).inflate(R.layout.custom_pay_now_alertdialog, null);
        ListView listView = (ListView) v.findViewById(R.id.listview);
        ArrayList<String> list = new ArrayList<>();
        list.add("Xpendz Wallet");
        list.add("Cash");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, list);
        listView.setAdapter(adapter);
        builder.setView(v);
        final AlertDialog dialog = builder.create();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        dialog.dismiss();
                        confirmPay("wallet", user, amount);
                        break;
                }
            }
        });
        dialog.show();
    }

    void confirmPay(String via, final User user, final int amount) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (via.equals("wallet")) {
            if (amount <= prefs.getUser().wallet) {
                builder.setMessage("Are you sure you want to pay Rs." + amount + " via Xpendz wallet?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        pay(user, amount);
                    }
                });
            } else {
                builder.setTitle("Error");
                builder.setMessage("Not enough Xpendz cash!");
            }
            builder.setNegativeButton("Cancel", null);
        }
        builder.create().show();
    }

    void pay(final User user, final int amount) {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait");
        progressDialog.show();
        String url = Config.URL + "/pay";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                View view = LayoutInflater.from(context).inflate(R.layout.custom_trans_success_layout, null);
                builder.setView(view);
                final Button button = (Button) view.findViewById(R.id.done);
                final AlertDialog dialog = builder.create();
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        prefs.payTo(user.id, prefs.alreadyPaidTo(user.id) + amount);
                        list.remove(user);
                        MainPageAdapter.this.notifyDataSetChanged();
                        MainActivity.wallet.setText("Rs." + String.valueOf(prefs.getUser().wallet - amount));
                        dialog.dismiss();
                    }
                });
                dialog.show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("option", error.toString());
                Toast.makeText(context, "Connection Timeout", Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("from_id", String.valueOf(prefs.getUser().id));
                params.put("to_id", String.valueOf(user.id));
                params.put("amount", String.valueOf(amount));
                return params;
            }
        };
        VolleySingleton.getInstance().getRequestQueue().add(stringRequest);
    }
}
