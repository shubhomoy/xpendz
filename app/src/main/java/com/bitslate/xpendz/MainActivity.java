package com.bitslate.xpendz;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.bitslate.xpendz.Adapters.MainPagerAdapter;
import com.bitslate.xpendz.Fragments.ItemsFragment;
import com.bitslate.xpendz.Fragments.RecordsFragment;
import com.bitslate.xpendz.Objects.Item;
import com.bitslate.xpendz.Objects.User;
import com.bitslate.xpendz.Services.NotificationFetch;
import com.bitslate.xpendz.Utils.Config;
import com.bitslate.xpendz.Utils.Preference;
import com.bitslate.xpendz.Utils.VolleySingleton;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private CoordinatorLayout root;
    private Toolbar toolbar;
    private FloatingActionButton fab;

    private TabLayout mTablayout;
    public static ViewPager pager;
    private String[] tabs;
    public static Preference prefs;
    private ArrayList<Fragment> fragments;
    private MainPagerAdapter pagerAdapter;
    static RecordsFragment recordsFragment;
    ItemsFragment itemsFragment;
    TextView hintText;
    RelativeLayout enterItemDialog;

    private TextInputLayout titleMsg, titleCost;
    private EditText itemName, itemCost;
    private ImageView helpBtn;
    private TextView helpText;
    public static TextView wallet;

    public static Context context;

    private TextView quickPay, groupPay, scanQr, addMoney;

    public static Activity mainActivity;


    Menu menu;

    boolean helpVisible = false, itemDialogEnter = false;

    public void instantiate() {
        mainActivity = this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        context = MainActivity.this;
        getSupportActionBar().setTitle("");

        enterItemDialog = (RelativeLayout) findViewById(R.id.insert_item_dialog);
        root = (CoordinatorLayout) findViewById(R.id.coordinator_layout);
        mTablayout = (TabLayout) findViewById(R.id.tab_layout);
        pager = (ViewPager) findViewById(R.id.pager);

        quickPay = (TextView) findViewById(R.id.quick_pay);
        groupPay = (TextView) findViewById(R.id.group_pay);
        scanQr = (TextView) findViewById(R.id.scan_qr);
        addMoney = (TextView) findViewById(R.id.add_cash);
        wallet = (TextView) findViewById(R.id.wallet_text);

        quickPay.setOnClickListener(this);
        groupPay.setOnClickListener(this);
        scanQr.setOnClickListener(this);
        addMoney.setOnClickListener(this);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        tabs = getResources().getStringArray(R.array.tabs);
        hintText = (TextView) findViewById(R.id.hint_text);
        hintText.setText(Html.fromHtml("\"This list displays the net amount you <font color='#DD2C00'><b>borrowed</b></font> or <font color='#1B5E20'><b>lent</b></font> for each of your friend."));
        titleMsg = (TextInputLayout) findViewById(R.id.text_layout_name);
        titleCost = (TextInputLayout) findViewById(R.id.text_layout_cost);
        itemName = (EditText) findViewById(R.id.item_name);
        itemCost = (EditText) findViewById(R.id.cost);
        helpBtn = (ImageView) findViewById(R.id.help_btn);
        helpText = (TextView) findViewById(R.id.item_help_text);
        recordsFragment = new RecordsFragment();
        itemsFragment = new ItemsFragment();

        fragments = new ArrayList<>();
        fragments.add(recordsFragment);
        fragments.add(itemsFragment);

        prefs = new Preference(this);
        wallet.setText("Rs." + prefs.getUser().wallet);
        startService(new Intent(this, NotificationFetch.class));
        fetchUser();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_page);

        instantiate();

        pagerAdapter = new MainPagerAdapter(getSupportFragmentManager(), tabs, fragments);
        pager.setAdapter(pagerAdapter);

        mTablayout.setTabsFromPagerAdapter(pagerAdapter);
        mTablayout.setupWithViewPager(pager);
        CustomOnPageChangeListener customOnPageChangeListener = new CustomOnPageChangeListener();
        pager.addOnPageChangeListener(customOnPageChangeListener);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animator Anim = null;
                if (itemDialogEnter) {

                    final String name = itemName.getText().toString().trim();
                    final String cost = "" + itemCost.getText().toString().trim();

                    if (name.matches("") && cost.matches("")) {
                        titleMsg.setError("Title cannot be blank");
                        titleCost.setError("Cost is blank");
                    } else if (name.matches("")) {
                        titleMsg.setError("Title cannot be blank");
                        titleCost.setError(null);
                    } else if (cost.matches("")) {
                        titleMsg.setError(null);
                        titleCost.setError("Cost is blank");
                    } else {
                        pager.setVisibility(View.VISIBLE);
                        setviewNull();
                        int cx = enterItemDialog.getLeft() + enterItemDialog.getRight();
                        int CY = enterItemDialog.getBottom();
                        int finalRadius = 0;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            Anim = ViewAnimationUtils.createCircularReveal(enterItemDialog, cx, CY, enterItemDialog.getHeight() + 1000, finalRadius);
                        }
                        Anim.setDuration(500).setInterpolator(new DecelerateInterpolator(1));
                        Anim.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                enterItemDialog.setVisibility(View.INVISIBLE);
                            }
                        });
                        Anim.start();
                        fab.hide();
                        itemDialogEnter = false;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                fab.setImageResource(R.mipmap.ic_add_white_36dp);
                                fab.show();
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        addItem(name, cost);
                                    }
                                }, 300);
                            }
                        }, 500);
                    }

                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        itemDialogEnter = true;
                        onPrepareOptionsMenu(menu);
                        pager.setVisibility(View.GONE);
                        int cx = enterItemDialog.getLeft() + enterItemDialog.getRight();
                        int CY = enterItemDialog.getBottom();
                        int finalRadius = Math.max(enterItemDialog.getWidth(), enterItemDialog.getHeight() + 1000);
                        Anim = ViewAnimationUtils.createCircularReveal(enterItemDialog, cx, CY, 0, finalRadius);
                        Anim.setDuration(500).setInterpolator(new DecelerateInterpolator(1));
                        enterItemDialog.setVisibility(View.VISIBLE);
                        Anim.start();
                        fab.hide();
                        helpBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!helpVisible) {
                                    helpText.setVisibility(View.VISIBLE);
                                    helpVisible = true;
                                } else {
                                    helpText.setVisibility(View.GONE);
                                    helpVisible = false;
                                }
                            }
                        });

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                fab.setImageResource(R.mipmap.ic_done_white_36dp);
                                fab.show();
                            }
                        }, 500);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        final View view = LayoutInflater.from(context).inflate(R.layout.custom_dialog_item, null);
                        final EditText itemName = (EditText) view.findViewById(R.id.item_name);
                        final EditText itemCost = (EditText) view.findViewById(R.id.cost);
                        builder.setView(view);
                        builder.setTitle("Add Item");
                        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (itemName.getText().toString().trim().length() > 0 && itemCost.getText().toString().trim().length() > 0)
                                    addItem(itemName.getText().toString(), itemCost.getText().toString());
                            }
                        });
                        builder.setNegativeButton("Cancel", null);
                        builder.setNeutralButton("Help", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                showAddItemHelp();
                            }
                        });
                        builder.create().show();
                    }
                }
            }
        });
    }

    public static void fetchUser() {
        String url = Config.URL + "/profile?user_id=" + prefs.getUser().id;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = new Gson();
                try {
                    User user = gson.fromJson(response.getString("user"), User.class);
                    prefs.setUser(user);
                    wallet.setText("Rs. " + prefs.getUser().wallet);
                } catch (JSONException e) {
                    Toast.makeText(context, "Connection Timeout", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("option", error.toString());
                Toast.makeText(context, "Connection Timeout", Toast.LENGTH_LONG).show();
            }
        });
        VolleySingleton.getInstance().getRequestQueue().add(jsonObjectRequest);
    }

    void showAddItemHelp() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle("Add Item - Help");
        builder.setMessage("Add items where you and your friends have spent. Give the item a name, such as, Movie, Food, etc and enter the total amount spent there.");
        builder.setPositiveButton("Ok", null);
        builder.create().show();
    }

    private void addItem(final String name, final String cost) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Adding item");
        progressDialog.show();
        String url = Config.URL + "/add/item";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Gson gson = new Gson();
                    Item item = gson.fromJson(jsonObject.getString("item"), Item.class);
                    Intent intent = new Intent(MainActivity.this, SplitActivity.class);
                    intent.putExtra("item_id", item.id);
                    intent.putExtra("cost", item.cost);
                    intent.putExtra("item_name", item.name);
                    startActivityForResult(intent, 1);
                } catch (JSONException e) {
                    Snackbar.make(root, "Connection Timeout", Snackbar.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.d("option", error.toString());
                Snackbar.make(root, "Connection Timeout", Snackbar.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("name", name);
                params.put("cost", cost);
                params.put("user_id", String.valueOf(prefs.getUser().id));
                return params;
            }
        };
        VolleySingleton.getInstance().getRequestQueue().add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        this.menu = menu;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.about:
                startActivity(new Intent(context, AboutUsActivity.class));
                break;
            case R.id.logoff:
                logoff();
                break;
            case R.id.reminder:
                startActivity(new Intent(this, ReminderActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showGroupPay() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.custom_group_pay, null);
        builder.setView(view);
        final TextView createGroup, payGroup;

        createGroup = (TextView) view.findViewById(R.id.export_text);
        payGroup = (TextView) view.findViewById(R.id.import_text);
        final AlertDialog dialog = builder.create();
        createGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view1) {
                dialog.dismiss();
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                View v = LayoutInflater.from(context).inflate(R.layout.custom_create_group_dialog, null);
                final EditText myShareEt = (EditText) v.findViewById(R.id.your_share_et);
                final EditText totalAmountEt = (EditText) v.findViewById(R.id.total_amount_et);
                final Button cancel = (Button) v.findViewById(R.id.cancel);
                final Button createGroup = (Button) v.findViewById(R.id.create);
                builder.setView(v);

                final AlertDialog groupDialog = builder.create();
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        groupDialog.dismiss();
                    }
                });

                createGroup.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        groupDialog.dismiss();
                        Intent intent = new Intent(context, GroupCreatedActivity.class);
                        intent.setAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
                        intent.putExtra("my_share", Integer.parseInt(myShareEt.getText().toString().trim()));
                        intent.putExtra("total_amount", Integer.parseInt(totalAmountEt.getText().toString().trim()));
                        startActivity(intent);
                    }
                });
                groupDialog.show();
            }
        });

        payGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivity(new Intent(context, PayToGroupActivity.class));
            }
        });
        dialog.show();
    }

    public void showSnackBar(String message) {
        Snackbar.make(root, message, Snackbar.LENGTH_LONG).show();
    }

    public static void refreshRecords() {
        recordsFragment.swipeRefreshLayout.setRefreshing(true);
        recordsFragment.swipeRefreshLayout.setEnabled(false);
        recordsFragment.showReport();
    }

    private void logoff() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Log out");
        builder.setMessage("Log out from this device?");
        builder.setPositiveButton("Sure", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                prefs.clear();
                startActivity(new Intent(context, LoginActivity.class));
                finish();
            }
        });
        builder.setNegativeButton("I'll stay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.create().show();

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem reminder = menu.findItem(R.id.reminder);
        MenuItem about = menu.findItem(R.id.about);
        MenuItem logoff = menu.findItem(R.id.logoff);
        if (!itemDialogEnter) {
            // enable
            reminder.setEnabled(true);
            about.setEnabled(true);
            logoff.setEnabled(true);
        } else {
            //disable
            reminder.setEnabled(false);
            about.setEnabled(false);
            logoff.setEnabled(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            recordsFragment.swipeRefreshLayout.setRefreshing(true);
            itemsFragment.swipeRefreshLayout.setRefreshing(true);
            recordsFragment.showReport();
            itemsFragment.showItems();
            pager.setCurrentItem(0, true);
        } catch (Exception e) {
            showSnackBar("Swipe to refresh");
        }
    }

    @Override
    public void onBackPressed() {
        if (itemDialogEnter) {
            pager.setVisibility(View.VISIBLE);
            Animator Anim = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                setviewNull();
                helpVisible = false;
                helpText.setVisibility(View.GONE);
                int cx = enterItemDialog.getLeft() + enterItemDialog.getRight();
                int CY = enterItemDialog.getBottom();
                int finalRadius = 0;
                Anim = ViewAnimationUtils.createCircularReveal(enterItemDialog, cx, CY, enterItemDialog.getHeight() + 1000, finalRadius);
                Anim.setDuration(500).setInterpolator(new DecelerateInterpolator(1));
                Anim.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        enterItemDialog.setVisibility(View.INVISIBLE);
                    }
                });
                Anim.start();
                fab.hide();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fab.setImageResource(R.mipmap.ic_add_white_36dp);
                        fab.show();
                    }
                }, 500);
            } else {
                enterItemDialog.setVisibility(View.INVISIBLE);
            }
            itemDialogEnter = false;
            onPrepareOptionsMenu(menu);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.quick_pay:
                startActivity(new Intent(context, QuickPayActivity.class).putExtra("fromMainActivity", true));
                break;
            case R.id.group_pay:
                showGroupPay();
                break;
            case R.id.add_cash:
                startActivity(new Intent(context, AddWalletActivity.class));
                break;
            case R.id.scan_qr:
                startActivity(new Intent(context, ScanQRActivity.class));
                break;
        }
    }

    private class CustomOnPageChangeListener extends ViewPager.SimpleOnPageChangeListener {

        @Override
        public void onPageSelected(int position) {

            switch (position) {
                case 0:
                    hintText.setText(Html.fromHtml("\"This list displays the net amount you <font color='#DD2C00'><b>borrowed</b></font> or <font color='#1B5E20'><b>lent</b></font> for each of your friend."));
                    break;
                case 1:
                    hintText.setText("This list displays all the items you have created or your friends have added you.");
                    break;
            }
            super.onPageSelected(position);
        }

    }

    void setviewNull() {
        titleMsg.setError(null);
        titleCost.setError(null);
        itemName.setText(null);
        itemCost.setText(null);
    }
}