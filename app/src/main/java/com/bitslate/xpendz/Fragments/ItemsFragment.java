package com.bitslate.xpendz.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bitslate.xpendz.Adapters.ItemsActivityAdapter;
import com.bitslate.xpendz.MainActivity;
import com.bitslate.xpendz.Objects.Item;
import com.bitslate.xpendz.R;
import com.bitslate.xpendz.Utils.Config;
import com.bitslate.xpendz.Utils.Preference;
import com.bitslate.xpendz.Utils.VolleySingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ItemsFragment extends Fragment {

    private RecyclerView recyclerView;
    private LinearLayout emptyView;
    ArrayList<Item> list;
    Preference prefs;
    ItemsActivityAdapter adapter;
    public SwipeRefreshLayout swipeRefreshLayout;

    public ItemsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_items, container, false);
        instantiate(v);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                swipeRefreshLayout.setEnabled(false);
                showItems();
            }
        });
        return v;
    }


    private void instantiate(View v) {
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        emptyView = (LinearLayout) v.findViewById(R.id.empty_view);
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipeRefreshLayout);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        prefs = new Preference(getActivity());
        list = new ArrayList<Item>();
        adapter = new ItemsActivityAdapter(getActivity(), list);
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout.setEnabled(false);
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });
        showItems();
    }

    public void showItems() {
        list.removeAll(list);
        list.clear();
        String url = Config.URL + "/show/items?user_id=" + prefs.getUser().id;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                swipeRefreshLayout.setRefreshing(false);
                swipeRefreshLayout.setEnabled(true);
                try {
                    JSONArray jsonArray = new JSONArray(response.getString("items"));
                    Gson gson = new Gson();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Item item = gson.fromJson(jsonArray.getJSONObject(i).toString(), Item.class);
                        list.add(item);
                    }
                    adapter.notifyDataSetChanged();
                    if (list.size() > 0) {
                        emptyView.setVisibility(View.GONE);
                    } else {
                        emptyView.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    //Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
                    ((MainActivity) getActivity()).showSnackBar("Something went wrong");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);
                swipeRefreshLayout.setEnabled(true);
                Log.d("option", error.toString());
                //Toast.makeText(getActivity(), "Connection Timeout", Toast.LENGTH_LONG).show();
                ((MainActivity) getActivity()).showSnackBar("Connection Timeout");
            }
        });
        VolleySingleton.getInstance().getRequestQueue().add(jsonObjectRequest);
    }

    public void retryLoading() {
        showItems();
    }
}
