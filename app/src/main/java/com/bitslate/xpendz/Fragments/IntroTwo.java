package com.bitslate.xpendz.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bitslate.xpendz.R;

/**
 * Created by shubhomoy on 11/1/16.
 */
public class IntroTwo extends Fragment{
    public IntroTwo() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_intro_two, container, false);

        return view;
    }

}
