package com.bitslate.xpendz.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.bitslate.xpendz.IntroActivity;
import com.bitslate.xpendz.LoginActivity;
import com.bitslate.xpendz.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class IntroOne extends Fragment {


    public IntroOne() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_intro_one, container, false);

        return view;
    }

}
