package com.bitslate.xpendz.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bitslate.xpendz.Adapters.MainPageAdapter;
import com.bitslate.xpendz.MainActivity;
import com.bitslate.xpendz.Objects.Item;
import com.bitslate.xpendz.Objects.User;
import com.bitslate.xpendz.R;
import com.bitslate.xpendz.Utils.Config;
import com.bitslate.xpendz.Utils.Preference;
import com.bitslate.xpendz.Utils.VolleySingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecordsFragment extends Fragment {

    private RecyclerView recyclerView;
    private LinearLayout emptyView;
    ArrayList<User> list;
    Preference prefs;
    MainPageAdapter adapter;
    public SwipeRefreshLayout swipeRefreshLayout;


    public RecordsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_records, container, false);
        instantiate(v);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                swipeRefreshLayout.setEnabled(false);
                showReport();
            }
        });
        return v;
    }


    void instantiate(View v) {
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        emptyView = (LinearLayout) v.findViewById(R.id.empty_view);
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipeRefreshLayout);
        prefs = new Preference(getActivity());
        list = new ArrayList<User>();
        adapter = new MainPageAdapter(getActivity(), list);
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout.setEnabled(false);
        showReport();
    }

    public void showReport() {
        list.removeAll(list);
        list.clear();
        String url = Config.URL + "/show/items?user_id=" + prefs.getUser().id;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                swipeRefreshLayout.setRefreshing(false);
                swipeRefreshLayout.setEnabled(true);
                ArrayList<Item> itemList = new ArrayList<>();
                Gson gson = new Gson();
                try {
                    JSONArray jsonArray = new JSONArray(response.getString("items"));
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Item item = gson.fromJson(jsonArray.getJSONObject(i).toString(), Item.class);
                        itemList.add(item);
                    }
                    setEmptyView(jsonArray.length());
                    runAlgo(itemList);
                } catch (JSONException e) {
                    //Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
                    ((MainActivity) getActivity()).showSnackBar("Something went wrong");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);
                swipeRefreshLayout.setEnabled(true);
                Log.d("option", error.toString());
                //Toast.makeText(getActivity(), "Connection timeout", Toast.LENGTH_LONG).show();
                ((MainActivity) getActivity()).showSnackBar("Connection Timeout");
            }
        });
        VolleySingleton.getInstance().getRequestQueue().add(jsonObjectRequest);
    }

    void runAlgo(ArrayList<Item> itemList) {
        for (int i = 0; i < itemList.size(); i++) {
            Item item = itemList.get(i);
            float curr_balance = 0;
            for (int j = 0; j < item.users.size(); j++) {
                User user = item.users.get(j);
                if (user.id == prefs.getUser().id) {
                    curr_balance = user.pivot.share - user.pivot.paid;
                    break;
                }
            }
            if (curr_balance == 0)
                continue;
            for (int j = 0; j < item.users.size(); j++) {
                if (curr_balance == 0)
                    break;
                User user = item.users.get(j);
                if (user.id != prefs.getUser().id) {
                    user.curr_balance += user.pivot.share - user.pivot.paid;
                    if (user.curr_balance < 0 && curr_balance > 0) {
                        if (user.curr_balance + curr_balance <= 0) {
                            user.my_stat += -curr_balance;
                            curr_balance = 0;
                        } else {
                            user.my_stat += user.curr_balance;
                            curr_balance -= -user.curr_balance;
                        }
                    } else if (user.curr_balance > 0 && curr_balance < 0) {
                        if (curr_balance + user.curr_balance < 0) {
                            user.my_stat += +user.curr_balance;
                            curr_balance += user.curr_balance;
                        } else {
                            user.my_stat += -curr_balance;
                            curr_balance = 0;
                        }
                    }
                    boolean flag = false;
                    for (int k = 0; k < list.size(); k++) {
                        if (user.id == list.get(k).id) {
                            list.get(k).my_stat += user.my_stat;
                            flag = true;
                            break;
                        }
                    }

                    if (flag == false) {
                       // user.my_stat += prefs.alreadyPaidTo(user.id);
                        //if(user.my_stat != 0)
                            list.add(user);
                    }
                }
            }
        }
        adapter.notifyDataSetChanged();
    }

    void setEmptyView(int length) {
        if (length == 0)
            emptyView.setVisibility(View.VISIBLE);
        else if (length > 0)
            emptyView.setVisibility(View.GONE);
    }

    public void retryLoading() {
        showReport();

    }
}
