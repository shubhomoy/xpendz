package com.bitslate.xpendz;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bitslate.xpendz.Objects.User;
import com.bitslate.xpendz.Utils.Config;
import com.bitslate.xpendz.Utils.Preference;
import com.bitslate.xpendz.Utils.VolleySingleton;
import com.bitslate.xpendz.Utils.XpendzGCM;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class RegistrationActivity extends AppCompatActivity {

    EditText nameEt;
    EditText phoneEt;
    EditText passwordEt;
    RelativeLayout signUpBtn;
    ProgressBar progressBar;
    TextView login;
    Preference prefs;

    void instantiate() {
        nameEt = (EditText) findViewById(R.id.name_et);
        phoneEt = (EditText) findViewById(R.id.phone_et);
        passwordEt = (EditText) findViewById(R.id.password_et);
        signUpBtn = (RelativeLayout) findViewById(R.id.signup_btn);
        prefs = new Preference(this);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        progressBar.getIndeterminateDrawable().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        login = (TextView) findViewById(R.id.login);

        Typeface regularTypeface = Typeface.createFromAsset(getAssets(), "fonts/regular.ttf");
        nameEt.setTypeface(regularTypeface);
        phoneEt.setTypeface(regularTypeface);
        login.setTypeface(regularTypeface);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        instantiate();


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegistrationActivity.this, LoginActivity.class));
                finish();
            }
        });

        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imn = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imn.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

                onRegistered(true);
                if (nameEt.getText().toString().trim().length() > 0 && phoneEt.getText().toString().trim().length() > 0 && passwordEt.getText().toString().trim().length() > 0) {
                    String url = Config.URL + "/signup";
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            onRegistered(false);
                            Gson gson = new Gson();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getString("msg").equals("already_exists")) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(RegistrationActivity.this);
                                    builder.setTitle("User exists");
                                    builder.setMessage("User already exists. Try login");
                                    builder.setPositiveButton("Ok", null);
                                    builder.create().show();
                                } else {
                                    User user = gson.fromJson(jsonObject.getString("user"), User.class);
                                    prefs.setUser(user);
                                    startActivity(new Intent(RegistrationActivity.this, MainActivity.class));
                                    finish();
                                    //new RegisterInBg().execute();
                                }
                            } catch (JSONException e) {
                                onRegistered(false);
                                Toast.makeText(RegistrationActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            onRegistered(false);
                            Log.d("option", error.toString());
                            Toast.makeText(RegistrationActivity.this, "Connection Timeout", Toast.LENGTH_LONG).show();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            HashMap<String, String> params = new HashMap<>();
                            params.put("name", nameEt.getText().toString());
                            params.put("phone", phoneEt.getText().toString());
                            params.put("password", passwordEt.getText().toString());
                            return params;
                        }
                    };
                    VolleySingleton.getInstance().getRequestQueue().add(stringRequest);
                } else {
                    onRegistered(false);
                }
            }
        });
    }

    private void onRegistered(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
            nameEt.setClickable(false);
            phoneEt.setClickable(false);
            signUpBtn.setClickable(false);
        } else {
            progressBar.setVisibility(View.GONE);
            nameEt.setClickable(true);
            phoneEt.setClickable(true);
            signUpBtn.setClickable(true);
        }

    }

    public class RegisterInBg extends AsyncTask<Void, Void, String> {
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(RegistrationActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setTitle("Registering");
            progressDialog.setMessage("Please wait");
        }

        @Override
        protected String doInBackground(Void... voids) {
            String msg = "";

            try {
                if (XpendzGCM.gcm == null) {
                    XpendzGCM.gcm = GoogleCloudMessaging.getInstance(RegistrationActivity.this);
                }
                XpendzGCM.regid = XpendzGCM.gcm.register(XpendzGCM.SENDER_ID);
                msg = "1";
                SharedPreferences prefs = getSharedPreferences("xpendz", Context.MODE_PRIVATE);
                int appVersion = XpendzGCM.getAppVersion(RegistrationActivity.this);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(XpendzGCM.PROPERTY_REG_ID, XpendzGCM.regid);
                editor.putInt("app_version", appVersion);
                editor.commit();
            } catch (IOException ex) {
                msg = "0";
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s.isEmpty()) {
                if (progressDialog.isShowing()) progressDialog.dismiss();
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(RegistrationActivity.this);
                builder.setTitle("Slow connection");
                builder.setCancelable(false);
                builder.setMessage("Registration was incomplete. Try again?");
                builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        new RegisterInBg().execute();
                    }
                });
                builder.create().show();
            } else if (s.equals("1")) {
                completeRegistration();
            }
        }
    }

    void completeRegistration() {
        String url = Config.URL+"/insertGcm";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                startActivity(new Intent(RegistrationActivity.this, MainActivity.class));
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("option", error.toString());
                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(RegistrationActivity.this);
                builder.setTitle("Retry?");
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        completeRegistration();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        onRegistered(false);
                        dialogInterface.dismiss();
                    }
                });
                builder.create().show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("user_id", String.valueOf(prefs.getUser().id));
                params.put("gcm_id", prefs.getGcmId());
                return params;
            }
        };
        VolleySingleton.getInstance().getRequestQueue().add(stringRequest);
    }
}
