package com.bitslate.xpendz.Services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bitslate.xpendz.MainActivity;
import com.bitslate.xpendz.Utils.Config;
import com.bitslate.xpendz.Utils.Preference;
import com.bitslate.xpendz.Utils.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by shubhomoy on 14/3/16.
 */
public class NotificationFetch extends Service {

    Handler handler;
    String url;
    Runnable runnable;
    Preference prefs;
    private NotificationManager mNotificationManager;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        prefs = new Preference(this);
        handler = new Handler();
        url = Config.URL + "/fetch/notifications?user_id="+prefs.getUser().id;
        runnable = new Runnable() {
            @Override
            public void run() {
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getString("msg").equals("valid")) {
                                if(MainActivity.mainActivity!=null) {
                                    MainActivity.fetchUser();
                                }
                                mNotificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
                                Intent i = new Intent(NotificationFetch.this, MainActivity.class);
                                PendingIntent contentIntent = PendingIntent.getActivity(NotificationFetch.this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
                                NotificationCompat.Builder mBuilder =
                                        new NotificationCompat.Builder(NotificationFetch.this)
                                                .setSmallIcon(android.R.drawable.stat_notify_chat)
                                                .setContentTitle("Xpendz")
                                                .setStyle(new NotificationCompat.BigTextStyle()
                                                        .bigText(response.getString("data")))
                                                .setLights(Color.WHITE, 2000, 2000)
                                                .setVibrate(new long[]{0, 300, 300, 300})
                                                .setContentText(response.getString("data"));
                                mBuilder.setContentIntent(contentIntent);
                                mNotificationManager.notify(1, mBuilder.build());
                            }
                        } catch (JSONException e) {

                        }
                        handler.postDelayed(runnable, 1000);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("option", error.toString());
                    }
                });
                VolleySingleton.getInstance().getRequestQueue().add(jsonObjectRequest);
            }
        };

        runnable.run();
    }
}
