package com.bitslate.xpendz;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bitslate.xpendz.Utils.Config;
import com.bitslate.xpendz.Utils.Preference;
import com.bitslate.xpendz.Utils.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class QuickPayActivity extends AppCompatActivity implements NfcAdapter.CreateNdefMessageCallback, NfcAdapter.OnNdefPushCompleteCallback {

    private EditText amount;
    Preference prefs;
    private Context context;

    private NfcAdapter nfcAdapter;
    private PackageManager packageManager;
    private CardView lendCard, borrowCard;
    Button payBtn;
    public static TextView lendAmount, lendCode;

    Toolbar toolbar;

    private void instantiate() {
        context = QuickPayActivity.this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Quick Pay");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        amount = (EditText) findViewById(R.id.amount);
        nfcAdapter = NfcAdapter.getDefaultAdapter(context);
        packageManager = this.getPackageManager();
        prefs = new Preference(context);
        lendCard = (CardView) findViewById(R.id.lend_card);
        borrowCard = (CardView) findViewById(R.id.borrow_card);
        lendAmount = (TextView) findViewById(R.id.lend_amount);
        lendCode = (TextView) findViewById(R.id.code);
        payBtn = (Button)findViewById(R.id.pay_btn);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_pay);
        instantiate();

        if (!packageManager.hasSystemFeature(PackageManager.FEATURE_NFC))
            Toast.makeText(QuickPayActivity.this, "NO NFC FOUND!", Toast.LENGTH_SHORT).show();
        else {
            if (!nfcAdapter.isEnabled())
                Toast.makeText(QuickPayActivity.this, "Enable NFC", Toast.LENGTH_SHORT).show();
            else {
                nfcAdapter.setNdefPushMessageCallback(this, this);
                nfcAdapter.setOnNdefPushCompleteCallback(this, this);
            }
        }

        payBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view1) {
                AlertDialog.Builder builder = new AlertDialog.Builder(QuickPayActivity.this);
                View v = LayoutInflater.from(QuickPayActivity.this).inflate(R.layout.custom_number_dialog, null);
                final EditText numberEt = (EditText)v.findViewById(R.id.number_et);
                builder.setView(v);
                builder.setPositiveButton("Pay", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String url = Config.URL + "/quickpaywithoutnfc";
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d("option", response);
                                AlertDialog.Builder builder = new AlertDialog.Builder(QuickPayActivity.this);
                                View view = LayoutInflater.from(QuickPayActivity.this).inflate(R.layout.custom_verify_item, null);
                                builder.setView(view);
                                builder.setCancelable(false);
                                final EditText code = (EditText) view.findViewById(R.id.code);
                                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                });

                                builder.setPositiveButton("Verify", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        sendCode(code.getText().toString());
                                    }
                                });
                                builder.create().show();
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("option", error.toString());
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                HashMap<String, String> params = new HashMap<String, String>();
                                params.put("from_id", String.valueOf(prefs.getUser().id));
                                params.put("to_number", numberEt.getText().toString().trim());
                                params.put("amount", amount.getText().toString().trim());
                                return params;
                            }
                        };
                        VolleySingleton.getInstance().getRequestQueue().add(stringRequest);
                    }
                });
                builder.setNegativeButton("Cancel", null);
                builder.create().show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        Intent intent = getIntent();
        Boolean fromMainActivity = intent.getBooleanExtra("fromMainActivity", false);
        if (fromMainActivity) {
            lendCard.setVisibility(View.VISIBLE);
            borrowCard.setVisibility(View.GONE);
        } else {
            lendCard.setVisibility(View.GONE);
            borrowCard.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {
        String text = "quickpay " + prefs.getUser().id + " " + amount.getText().toString();
        byte[] bytes = text.getBytes();

        NdefRecord record = new NdefRecord(
                NdefRecord.TNF_MIME_MEDIA,
                "text/plain".getBytes(),
                new byte[]{},
                bytes);
        return new NdefMessage(record);
    }

    @Override
    public void onNdefPushComplete(NfcEvent event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(QuickPayActivity.this);
                View view = LayoutInflater.from(QuickPayActivity.this).inflate(R.layout.custom_verify_item, null);
                builder.setView(view);
                builder.setCancelable(false);
                final EditText code = (EditText) view.findViewById(R.id.code);
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.setPositiveButton("Verify", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendCode(code.getText().toString());
                    }
                });
                builder.create().show();
            }
        });
    }

    private void sendCode(final String code) {
        String url = Config.URL + "/pay/sendcode";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                View view = LayoutInflater.from(context).inflate(R.layout.custom_trans_success_layout, null);
                builder.setView(view);
                builder.setCancelable(false);
                final Button button = (Button) view.findViewById(R.id.done);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MainActivity.fetchUser();
                        finish();
                    }
                });
                builder.create().show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("error", error.toString());
                Toast.makeText(QuickPayActivity.this, "Connection Timeout", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("code", code);
                params.put("from_id", String.valueOf(prefs.getUser().id));
                return params;
            }
        };
        VolleySingleton.getInstance().getRequestQueue().add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
