package com.bitslate.xpendz;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bitslate.xpendz.Adapters.GroupListAdapter;
import com.bitslate.xpendz.Objects.GroupMember;
import com.bitslate.xpendz.Utils.Config;
import com.bitslate.xpendz.Utils.Preference;
import com.bitslate.xpendz.Utils.VolleySingleton;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GroupCreatedActivity extends AppCompatActivity {

    private TextView totalAmount;
    public static Button payBtn;
    private RecyclerView recyclerView;
    public static GroupListAdapter adapter;
    private Intent intent;
    private Preference prefs;
    public static ArrayList<GroupMember> list;
    public static int amount, totAmt;
    private Context context;

    void instantiate() {
        prefs = new Preference(this);
        context = GroupCreatedActivity.this;
        totalAmount = (TextView) findViewById(R.id.total_amount_tv);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        payBtn = (Button) findViewById(R.id.pay_btn);
        intent = getIntent();
        totAmt = intent.getIntExtra("total_amount", 0);
        totalAmount.setText(String.valueOf(totAmt));
        list = new ArrayList<>();
        adapter = new GroupListAdapter(this, list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        list.add(new GroupMember(prefs.getUser().id, prefs.getUser().name, intent.getIntExtra("my_share", 0)));
        amount += intent.getIntExtra("my_share", 0);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_created);
        instantiate();

        payBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Gson gson = new Gson();
                final String json = gson.toJson(list);

                String url = Config.URL + "/group/collect";
                StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        View view = LayoutInflater.from(context).inflate(R.layout.custom_trans_success_layout, null);
                        builder.setView(view);
                        builder.setCancelable(false);
                        final Button button = (Button) view.findViewById(R.id.done);
                        button.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                MainActivity.fetchUser();
                                finish();
                            }
                        });
                        builder.create().show();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(GroupCreatedActivity.this, "UnSuccessful", Toast.LENGTH_SHORT).show();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        HashMap<String, String> params = new HashMap<>();
                        params.put("user_id", String.valueOf(prefs.getUser().id));
                        params.put("others", json);
                        return params;
                    }
                };
                VolleySingleton.getInstance().getRequestQueue().add(request);
            }
        });
    }


    public void removeItems(int position) {
        list.remove(position);
        adapter.notifyItemRemoved(position);
        adapter.notifyItemRangeChanged(position, list.size());
    }
}