package com.bitslate.xpendz.Objects;

/**
 * Created by shubhomoy on 13/3/16.
 */
public class GroupMember {
    public int id;
    public int amount;
    public String name;

    public GroupMember(int id, String name, int amount) {
        this.id = id;
        this.amount = amount;
        this.name = name;
    }
}
