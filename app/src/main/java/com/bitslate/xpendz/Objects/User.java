package com.bitslate.xpendz.Objects;

/**
 * Created by shubhomoy on 9/1/16.
 */
public class User {
    public int id;
    public String name;
    public String phone;
    public int wallet;
    public Pivot pivot;
    public int curr_balance;
    public int my_stat;

    public class Pivot {
        public int paid;
        public int share;
    }
}
