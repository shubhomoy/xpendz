package com.bitslate.xpendz.Objects;

/**
 * Created by ddvlslyr on 15/3/16.
 */
public class Event {
    public String id;
    public String code;
    public String event_name;
    public String event_description;
    public String price;
    public String created_at;
    public String updated_at;
}
