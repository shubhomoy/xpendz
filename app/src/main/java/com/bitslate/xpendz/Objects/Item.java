package com.bitslate.xpendz.Objects;

import java.util.ArrayList;

/**
 * Created by shubhomoy on 9/1/16.
 */
public class Item {
    public int id;
    public int user_id;
    public String name;
    public int cost;
    public ArrayList<User> users;
}
